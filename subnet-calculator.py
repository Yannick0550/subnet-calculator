#Subnet-Calculator von Matteo und Yannick
#Datum: 02/24

            #Formel/n
def log2(x):
    result = 0
    while x > 1:
        x /= 2
        result += 1
    return result

#######################################

#Schleife wird erstellt
wiederhole = True
while wiederhole == True:

    basis_netz=input("Bitte gib die Netzwerkadresse des Basisnetztes ein (z.B. 192.168.4.0):")
    arr_basis_netz=basis_netz.split(".") #IP wird in die einzelnen Oktette geteilt

    #zur Überprüfung
    #print(format(int(arr_basis_netz[0]),"b").zfill(8))
    #print(format(int(arr_basis_netz[1]), "b").zfill(8))
    #print(format(int(arr_basis_netz[2]), "b").zfill(8))
    #print(format(int(arr_basis_netz[3]), "b").zfill(8))

    anz_subnetz=int(input("Gebe bitte die Anzahl der gewünschten Subnetzte aus:"))
    log_anz_subnetz=log2(anz_subnetz)

    if log_anz_subnetz % 1 !=0:
        log_anz_subnetz= int(log_anz_subnetz)+1
    else:
        log_anz_subnetz=int(log_anz_subnetz)

    #zur Überprüfung
    #print(log_anz_subnetz)

    host_anz_subnetz=2 ** int(8-log_anz_subnetz)
    print("Anzahl der möglichen Host pro Netz:",host_anz_subnetz-2)
    subnetzmaske_laenge = 32 - (8 - log_anz_subnetz)  #Berechne die Länge des Netzwerkpräfixes basierend auf der Anzahl der Subnetze
    subnetzmaske_bin = '1' * subnetzmaske_laenge + '0' * (32 - subnetzmaske_laenge) #Subnetzmaske in binär
    subnetzmaske_dezimal = [str(int(subnetzmaske_bin[i:i + 8], 2)) for i in range(0, 32, 8)] #binär in dezimal umrechnen und Oktette teilen
    print("Die Subnetzmaske lautet:",'.'.join(subnetzmaske_dezimal)) #Gibt die Subnetzmaske aus

    for i in range(anz_subnetz):
        
        subnetz_prefix = int(log_anz_subnetz) #Anzahl der Hostbits in der Subnetzmaske
        new_subnetz_hostbits = 8 - subnetz_prefix # Anzahl der Hostbits in den neuen Subnetzwerken
        new_subnetz_hostanzahl = 2 ** new_subnetz_hostbits # Anzahl der Hosts in den neuen Subnetzwerken

        #Berechnung der Basisadresse für das neue Subnetz
        new_subnetz_basisadresse = arr_basis_netz.copy()
        new_subnetz_basisadresse[3] = str(int(new_subnetz_basisadresse[3]) + i * new_subnetz_hostanzahl)
        new_subnetz_basisadresse_str = '.'.join(new_subnetz_basisadresse)

        #Berechnung des IP-Bereichs für das neue Subnetz
        first_ip = new_subnetz_basisadresse_str
        last_ip = '.'.join(
            new_subnetz_basisadresse[:-1] + [str(int(new_subnetz_basisadresse[-1]) + new_subnetz_hostanzahl - 1)])
        
        print("IP-Bereich für das neue Netzwerk {}: {} - {}".format(i + 1, first_ip, last_ip))

        #Abfrage, ob eine weitere Rechnung durchegeführt werden soll
        #(es wird nur eine weitere Abfrage gemacht,wenn "ja" eingegeben wird).
    erneut = input("Soll nochmal was berechnet werden? (ja/nein) ")
    if erneut.lower() != "ja":
        wiederhole = 0
